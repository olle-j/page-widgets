import image from '@rollup/plugin-image'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import peerDepsExternal from 'rollup-plugin-peer-deps-external'
import terser from '@rollup/plugin-terser'
import copy from 'rollup-plugin-copy'
import css from 'rollup-plugin-import-css'
import json from '@rollup/plugin-json'
//import copy from "rollup-plugin-copy-assets"
// import url from '@rollup/plugin-url'
// import asset from "rollup-plugin-asset"

export default [
  {
    context: 'window',
    input: {
      index: 'src/index.js',
      // screenshot: 'src/screenshot/index.js',
      // carGame: 'src/widgets/carGame/index.js',
      // pong: 'src/widgets/pong/index.js',
      // sea: 'src/widgets/sea/index.js',
      // karaoke: 'src/widgets/karaoke/index.js'
    },
    output: [
      {
        dir: 'dist',
        format: 'esm',
  //      assetFileNames: "assets/[name]-[hash][extname]"
      }
    ],
    plugins: [
      image(),
      nodeResolve(),
      commonjs(),
      css(),
      json(),
      peerDepsExternal(),
      copy({
        targets: [
            {
              src: [
                'src/*.png',
                'src/**/*.png',
                'src/**/*.jpeg',
                'src/**/*.gif',
                'src/**/*.mp4',
                'src/**/*.midi',
                'src/**/*.mid',
                'src/**/*-ogg.js',
                'src/**/*-mp3.js'
              ],
              dest: 'dist'
            },
        ]
      }),
      terser()
    ],
  }
  ]

// adapted from p5.play example
// https://molleindustria.github.io/p5.play/examples/index.html?fileName=pong.js

import p5 from '../../lib/p5WithPlay'
import { takeScreenshot } from '../../screenshot'
import * as quadtree from '../../screenshot/quadTree.js'

const sketch = (s) => {
  console.group('credits')
  console.log('adapted from p5.play example')
  console.log('https://molleindustria.github.io/p5.play/examples/index.html?fileName=pong.js')
  console.groupEnd()

  let paddleA, paddleB, ball, wallTop, wallBottom, refImage
  let width, height, totalHeight, tree //, all
  const MAX_SPEED = 10
  const black70 = s.color(56,56,56)
  
  // short-term solution to allow page backgound colors
  const allowedColors = [[255, 255, 255]]

  s.setup = async () => {
    width = s.windowWidth
    totalHeight = document.documentElement.offsetHeight
    height = s.windowHeight

    const c = s.createCanvas(width, totalHeight)
    c.position(0, 0)
    document.getElementsByClassName('p5Canvas')[0].style.position = 'fixed'
    refImage = await takeScreenshot()
    tree = quadtree.fromImageData(refImage.data, width, 15, allowedColors)

    paddleA = s.createSprite(30, height / 2, 10, height / 6)
    paddleA.immovable = true

    paddleB = s.createSprite(width - 28, height / 2, 10, height / 6)
    paddleB.immovable = true

    wallTop = s.createSprite(width / 2, -30 / 2, width, 30)
    wallTop.immovable = true

    wallBottom = s.createSprite(width / 2, height + 30 / 2, width, 30)
    wallBottom.immovable = true

    ball = s.createSprite(width / 2, s.random(height), 10, 10)
    ball.maxSpeed = MAX_SPEED

    paddleA.shapeColor = paddleB.shapeColor = ball.shapeColor = black70

    ball.setSpeed(MAX_SPEED, -180)
  }

  s.draw = () => {
    if (!refImage) {
      return
    }
    s.clear()

    paddleA.position.y = s.constrain(s.mouseY, paddleA.height / 2, height - paddleA.height / 2)
    paddleB.position.y = s.constrain(s.mouseY, paddleA.height / 2, height - paddleA.height / 2)

    ball.bounce(wallTop)
    ball.bounce(wallBottom)

    const scrolled = Math.floor(document.documentElement.scrollTop || document.body.scrollTop || 0)
    const SEARCH_RADIUS = 10
    const obstacleSprites = quadtree.searchTree(
      tree,
      ball.position.x - SEARCH_RADIUS,
      ball.position.y + scrolled - SEARCH_RADIUS,
      ball.position.x + SEARCH_RADIUS,
      ball.position.y + scrolled + SEARCH_RADIUS
    ).map(([x, y]) => {
      const obstacle = s.createSprite(x, y + scrolled, 10, 10)
      obstacle.immovable = true
      if (ball.bounce(obstacle)) {
        // add a small random angle to the ball going out from the bounce.
        // this will help to prevent the ball getting stuck
        ball.setSpeed(MAX_SPEED, ball.getDirection() + Math.random())
      }
      return obstacle
    })

    let swing
    if (ball.bounce(paddleA)) {
      swing = (ball.position.y - paddleA.position.y) / 3
      ball.setSpeed(MAX_SPEED, ball.getDirection() + swing)
    }

    if (ball.bounce(paddleB)) {
      swing = (ball.position.y - paddleB.position.y) / 3
      ball.setSpeed(MAX_SPEED, ball.getDirection() - swing)
    }

    if (ball.position.x < 0) {
      ball.position.x = width / 2
      ball.position.y = s.random(height)
      ball.setSpeed(MAX_SPEED, 0)
    }

    if (ball.position.x > width) {
      ball.position.x = width / 2
      ball.position.y = s.random(height)
      ball.setSpeed(MAX_SPEED, 180)
    }

    s.drawSprite(paddleA)
    s.drawSprite(paddleB)
    s.drawSprite(ball)
    obstacleSprites.forEach(o => o.remove())
  }
}

const run = () => new p5(sketch)

const forOlleJohannessonDotCom = run
const standalone = run
const canRun = (bowser) => bowser.isPlatform('desktop')

export { forOlleJohannessonDotCom, standalone, canRun }

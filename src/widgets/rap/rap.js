import EasySpeech from 'easy-speech'
import * as drums from './drumloop'
import * as profilePic from '../../utils/profilepic/profilepic.js'
import { breakUpSyllables, replaceSeparatorsWithSpace, splitOnWordsButRetainSeparators } from './testParse'
import { spRegex } from './regexes'
import { rndChunk } from './grouping.js'
import { clamp } from '../../utils/jshelpers.js'
import rapOpen from 'url:./me_rap_open.png'
import rapClosed from 'url:./me_rap_closed.png'

let isPlaying = false
let voice
const log = []

export const openMouth = () => profilePic.changeTo(rapOpen)
export const closeMouth = () => profilePic.changeTo(rapClosed)

const onFail = () => {
    window.location.reload()
}

const speech = EasySpeech.detect()
if (!(speech.speechSynthesis && speech.speechSynthesisUtterance && speech.speechSynthesisEvent)) {
    onFail()
}

const ease = x => x < 3 ? ((1) / (2)) + ((Math.sin(((Math.PI) / (2)) * x - Math.PI)) / (2)) : 1

const sleep = (ms) => new Promise((r) => setTimeout(r, ms))

const onError = (err) => {
        log.push(err)
    }

const say = async (word = ',', numberOfSyllablesInWord = 1, pitch = 1) => {
    const rate = 1 + ease(numberOfSyllablesInWord)
    const part = numberOfSyllablesInWord > 1
        ? spRegex.exec(word)?.[0]
        : word
    EasySpeech.speak({
        pitch,
        text: part,
        voice,
        rate, // numberOfSyllablesInWord >= 2 ? 0.8 + numberOfSyllablesInWord * 0.2 : 1,
        start: openMouth,
        end: closeMouth,
    }).catch(({ message }) => {
        onError({
            message,
            part,
            numberOfSyllablesInWord,
            rate,
            voice,
            pitch,
            word,
            spRegex
        })
    })
}

const rap = async (text) => {
    stopCurrentSound()
    isPlaying = true
    voice = getRnd(EasySpeech.voices().filter(v => v.lang === 'en-US'))
    const quarterBeat = (bpm) => (1000 / bpm) * 60
    let pitch = 1
    let words = breakUpSyllables(replaceSeparatorsWithSpace(splitOnWordsButRetainSeparators(text))) //rndChunk(syllabledwords)
    words = rndChunk(words)
    await drums.play(stop)
    for (const word of words) {
        if (!isPlaying) {
            stopCurrentSound()
            break
        }
        const beatTime = quarterBeat(91)
        const pitchDelta = word.length / 10
        pitch = clamp(1, 2, 1 + pitchDelta)
        for (const syllable of word) {
            say(syllable, word.length, pitch)
            await sleep(beatTime / word.length)
        }
    }
    stop()
}

export const stopCurrentSound = () => {
    drums.stop()
    EasySpeech.cancel()
}

export const stop = () => {
    stopCurrentSound()
    isPlaying = false
    profilePic.restore()
}

const getRnd = a => a[Math.floor(Math.random() * a.length)]

export { rap, isPlaying }

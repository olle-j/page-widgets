It would, of course, be better to have p5 as a separate dependency. However, due to the unflexible nature of how p5.play adds itself to p5, the annoying way it has to be done in instance mode, and the opinionated location of the p5 addons, it is simpler this way.

Also, p5.play seems not to be regularily maintained, so I hope this will solve versioning problems.

For widgets that don't require p5.play, a regular dependency on p5 can be used.
import { quadtree } from 'd3-quadtree'
import { getForbiddenPoints, getFuzzyForbiddenPoints } from './imageUtils.js'

const trees = new Map()
let treeCount = 0

export const get = id => trees.get(id)

export const storePoints = (points) => {
  const tree = quadtree().addAll(points)
  const id = treeCount++
  trees.set(id, tree)
  return id
}

export const fromImageData = (imageData, width, tolerance, allowedColors) => {
  if (typeof tolerance === 'function' && !allowedColors) {
    allowedColors = tolerance
  }

  const data = typeof allowedColors === 'function'
    ? getForbiddenPoints(imageData, width, allowedColors)
    : getFuzzyForbiddenPoints(imageData, width, allowedColors, tolerance)

  return storePoints(data)
}

export const searchTree = (id, xmin, ymin, xmax, ymax, transform) => {
  return trees.has(id)
    ? search(trees.get(id), xmin, ymin, xmax, ymax, transform)
    : []
}

const search = (quadtree, xmin, ymin, xmax, ymax, transform) => {
  const results = []
  transform = transform || ((d) => d)
  quadtree.visit((node, x1, y1, x2, y2) => {
    if (!node.length) {
      do {
        const d = node.data
        if (d[0] >= xmin && d[0] < xmax && d[1] >= ymin && d[1] < ymax) {
          results.push(transform(d))
        }
      } while ((node = node.next))
    }
    return x1 >= xmax || y1 >= ymax || x2 < xmin || y2 < ymin
  })
  return results
}

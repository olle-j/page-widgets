import html2canvas from 'html2canvas'

export async function takeScreenshot(target = document.body) {
  return html2canvas(target, {
    scale: 1,
  }).then(canvas => {
    const ctx = canvas.getContext('2d')
    const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height)
    console.debug(imageData)
    return imageData
  })
}

export function setupDebugImage(s, imgData) {
  const debugImage = s.createImage(imgData.width, imgData.height)
  let showDebugImage = false

  debugImage.loadPixels() 
  for (let i = 0; i < imgData.data.length; i++) {
    debugImage.pixels[i] = imgData.data[i] // Directly copy pixel values
  }
  debugImage.updatePixels()

  window.toggleDebugImage = function () {
    showDebugImage = !showDebugImage
    console.log('Debug image visibility:', showDebugImage)
  }

  // Return a function to be used in `s.draw()`
  return function () {
    if (showDebugImage) {
      s.tint(150, 255, 150, 120) // Greenish tint with transparency
      s.image(debugImage, 0, 0, s.width, s.height)
      s.noTint()
    }
  }
}

export function setupObstacleDrawing(s, obstacles) {
  let showObstacles = false 

  console.log('defining toggleDrawAllObstacles')
  window.toggleDrawAllObstacles = function () {
    showObstacles = !showObstacles
    console.log('Obstacle visibility:', showObstacles)
  }

  // Return a function to be used in `s.draw()`
  return function () {
    if (showObstacles) {
      s.fill(255, 0, 0)
      s.noStroke()
      for (const obstacle of obstacles) {
        s.rect(obstacle.position.x, obstacle.position.y, 1, 1)
      }
    }
  }
}

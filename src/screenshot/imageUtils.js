import { arrayEquals } from '../utils/arrayEquals'
import { compareArrayWithTolerance } from '../utils/fuzzy.js'

// inverted JS true/false values to simplify integral image
// with the assumption that the bg colors are the most common
export function makeBoolImage(image, allowedColors) {
  const boolImage = new Uint8Array(image.length / 4)

  for (let i = 0, j = 0, l = image.length; i < l; i += 4, j += 1) {
    const col = image.slice(i, i + 3)
    if (!allowedColors.some(allowed => arrayEquals(allowed, col))) {
      boolImage[j] = 1
    }
  }

  return boolImage
}

export function getForbiddenPointsThroughBoolImage(image, width, allowedColors) {
  const res = []
  const boolImage = makeBoolImage(image, allowedColors)
  for (let i = 0, l = boolImage.length; i < l; i++) {
    if (boolImage[i]) {
      res.push([Math.floor(i / width), i % width])
    }
  }
  return res
}

export function getForbiddenPoints(image, width, allowedColors) {
  const forbiddenPoints = []
  const withCallback = col => allowedColors(col)
  const withArrayOfColors = col => allowedColors.some(allowed => arrayEquals(allowed, col))
  const isAllowed = typeof allowedColors === 'function'
    ? withCallback
    : withArrayOfColors

  for (let i = 0, j = 0, l = image.length; i < l; i += 4, j += 1) {
    const col = image.slice(i, i + 3)
    if (!isAllowed(Array.from(col))) {
      forbiddenPoints.push([j % width, Math.floor(j / width)])
    }
  }

  return forbiddenPoints
}

export function getFuzzyForbiddenPoints(image, width, allowedColors, tolerance) {
  const forbiddenPoints = []
  const isAllowed = col => allowedColors.some(allowed => compareArrayWithTolerance(allowed, col, tolerance))

  for (let i = 0, j = 0, l = image.length; i < l; i += 4, j += 1) {
    const col = image.slice(i, i + 3)
    if (!isAllowed(Array.from(col))) {
      forbiddenPoints.push([j % width, Math.floor(j / width)])
    }
  }

  return forbiddenPoints
}

// export function makeBoolImageExtended(image, allowedColors, width, options) {
//   const boolImage = makeBoolImage(image, allowedColors)
//   const { horizontalMergeThresh, verticalMergeThresh } = options || {}
//   let horizontalStack = []
//   let verticalStack = []

//   const horizontalPush = i => {
//     if (horizontalStack.length <= horizontalMergeThresh) {
//       horizontalStack.push(i)
//     } else if (horizontalStack.length <= horizontalMergeThresh) {
//       horizontalStack = []
//     }
//     if (width - (i % width) >= horizontalMergeThresh) {
//       horizontalStack = []
//     }
//   }

//   const

//   for(let i = 0,
//           prevForbidden = false,
//           l = image.length
//           ; i < l
//           ; i += 1
//       ) {
//     if(prevForbidden) {
//       if (horizontalMergeThresh && width - (i % width) >= horizontalMergeThresh) {
//         if (horizontalStack.length > horizontalMergeThresh) {
//           horizontalStack.push(i)
//           horizontalPush = true
//         }

//       }

//     }

//     if (boolImage[i] === 1) {
//       for(m = 0, hs = horizontalStack.length; m < hs; m += 1) {
//         boolImage[m] = 1
//       }
//       horizontalStack = []
//     } else {
//       if (horizontalMergeThresh) {
//         horizontalPush(i)
//       }
//       prevForbidden = false
//     }
//   }

//   return boolImage
// }

export function makeIntegralImage(image, width, height) {
  const iImage = new Uint32Array(image.length + height + width + 1)

  for (let j = 0; j < height; j += 1) {
    for (let i = 0; i < width; i += 1) {
      const imageVal = image[i + j * width]
      const nw = iImage[i + j * (width + 1)]
      const n = iImage[(i + 1) + j * (width + 1)]
      const w = iImage[i + (j + 1) * (width + 1)]
      iImage[(i + 1) + (j + 1) * (width + 1)] = imageVal + n + w - nw
    }
  }

  return iImage
}

// get integralImage sum for a rect
// add 1 to all coordinates to account for initial
// 0-row and 0-column in integral image
export function rectContainsForbiddenColor(image, x1, y1, x2, y2, width) {
  const se = image[x2 + 1 + (y2 + 1) * (width + 1)]
  const nw = image[x1 + y1 * (width + 1)]
  const ne = image[x2 + 1 + y1 * (width + 1)]
  const sw = image[x1 + (y2 + 1) * (width + 1)]
  return Boolean(se - ne - sw + nw)
}

export function runLengthEncodeImage(image) {
  const a = []
  for (let i = 0, l = image.length; i < l;) {
    const val = image[i]
    let count = 1
    while (image[i + count] === val) {
      count += 1
    }
    a.push(count, val)
    i += count
  }
  return a
}

export function decodeRunLengthImage(rlImage) {
  let a = []
  for (let i = 0, l = rlImage.length; i < l; i += 2) {
    const run = rlImage[i]
    const value = rlImage[i + 1]
    a = a.concat(new Array(run).fill(value))
  }
  return a
}

export function sliceRunLengthImage(rlImage, from, to) {
  let startIndex = 0, startSum = rlImage[0]

  while (startSum < from) {
    startIndex += 2
    startSum += rlImage[startIndex]
  }

  let endIndex = startIndex, endSum = startSum - from

  while (endSum < to) {
    endIndex += 2
    endSum += rlImage[endIndex]
  }

  if (startIndex === endIndex) {
    return [Math.abs(from - to), rlImage[startIndex + 1]]
  }

  return [startSum - from, rlImage[startIndex + 1],
    ...rlImage.slice(startIndex + 2, endIndex),
    endSum - to, rlImage[endIndex + 1]]
}

export function splitRunLengthImageAt(rlImage, at) {
  let endIndex = 0, endSum = rlImage[0]

  while (sum < at) {
    // eslint-disable-next-line no-unused-vars
    endIndex += 2
    endSum += rlImage[startIndex]
  }

  const diff = endSum - at
}

/**
 * OK, now i know how to do this:
 *
 * We don't need to RL encode the entire picture, since we don't care about the
 * allowed pixels. The mismatch between space-complexity and time-complexity is
 * dealing with an enourmous amount of data points and quickly extracting them.
 * The game is efficient encoding for the usecase, and efficient local decoding.
 * So here's what I'm going to try next:
 *
 * in setup:
 * 1. Take screenshot
 * 2. Make boolean image
 * 3. Make an array of arrays –a Y-first matrix– encoding this:
 * [x-offset to start run of forbidden points, x-offset to stop of forbidden points, simile]
 * for every row.
 * It will now be fast to find the xy-offset for decoding start, and fast to "decode" a square
 * somewhere locally centered in image, and extract xy-coordinates of "obstacles"
 *
 * in draw:
 * 5. Make quadtree in desired square (center-rect mode should be a help here)
 * 4. Extract xy-coordinates of obstacles in desired square
 * 6. Enter xy-coordinates in quadtree
 * 7. collision detection
 * 8. $$$
 */

/**
 * Take a RGBA imageData data array and turn it into a collection of arrays representing its rows,
 * storing a kind of run/length encoding for forbidden pixels; the onset/offset x-coordinates of
 * such pixels. A row like [0,0,0,1,1,0,0,1,1,1,1,1,0] with a width of 6 should produce [[3,4],[1,5]]
 * This should have a comparable size-complexity overhead to a run/lenght-encoded bool image, reduced
 * to only run-indications with an initial start-state member (where the above example would
 * produce [0,3,2,2,5,1]), but facilitate much faster extraction of xy-coordinates for forbidden pixels.
 *
 * The two-dimensionality will produce some overhead compared with one-dimensional run/length, as
 * many rows with just the background-color would only produce a single entry to a run/length-encoding,
 * whereas this will produce a number of empty arrays, same as the number of image rows with
 * only the allowed color. But unless we are dealing with a seriously minimalistic webpage,
 * it should not be too disappointing.
 *
 * TODO: Make compression comparison graph using a canvases with increasing amounts of (realistic) data.
 * compare boolean image, run/length boolean, run/length-only-runs boolean, and RFOOI
 *
 * @param {*} image imageData data array
 * @param {*} width width of image
 * @param {*} allowedColors for now, an array of RGB arrays. In the future it should be a callback
 * @returns a Y-first matrix, the image row-wise with onset/offset values for forbidden pixels
 */
export function encodeRowwiseForbiddenOnsetOffsetImage(image, width, allowedColors) {
  //console.log("starting to encode")
  const height = image.length / width / 4
  const enc = new Array(height)
  for (let i = 0; i < height; i += 1) {
    enc[i] = []
  }

  for (let i = 0,
         xOffset = 0,
         yOffset = 0,
         previousWasAllowed = true,
         l = image.length
    ; i < l
    ; i += 4
  ) {
    xOffset = Math.floor(Math.floor(i / 4)) % width
    yOffset = Math.floor(Math.floor(i / 4) / width)

    const currentIsAllowed = allowedColors.some(allowed => arrayEquals(allowed, image.slice(i, i + 3)))

    if (currentIsAllowed) {
      if (!previousWasAllowed) {
        //console.log("end of run at x:", xOffset, " y:", yOffset)
        enc[yOffset].push(xOffset)
        previousWasAllowed = true
      }
    } else {
      if (previousWasAllowed) {
        //console.log("start of run at x:", xOffset, " y:", yOffset)
        enc[yOffset].push(xOffset)
        previousWasAllowed = false
      }
    }
  }
  //console.log("finished encoding")
  return enc
}

export function extractForbiddenCoordsFromRFOOI(RFOOI, yOffset = 0) {
  const coords = []
  //console.log(RFOOI) only empty array!

  // console.log(RFOOI.length)
  // console.log(RFOOI)
  for (let y = 0, h = RFOOI.length; y < h; y += 1) {
    for (let r = 0, w = RFOOI[y].length; r < w; r += 2) {
      for (let x = RFOOI[y][r]; x <= RFOOI[y][r + 1]; x += 1) {
        coords.push({ x, y: y + yOffset })
      }
    }
  }
  //console.log(coords.length)
  return coords
}

export function sliceRFOOIToRect(RFOOI, x1, y1, x2, y2) {
  const enc = []
  for (let y = y1, i = 0; y < y2; y += 1, i += 1) {
    enc[i] = sliceRFOIIRow(RFOOI[y], x1, x2)
  }
  //console.log(enc)
  return enc
}

/**
 * Slice a portion of a RFOII row, so that onset periods extending before and
 * after the limits are properly cut
 * @param {*} row
 * @param {*} x1
 * @param {*} x2
 * @returns
 */
export function sliceRFOIIRow(row, x1, x2) {
  // if the row is empty, return empty
  if (!row.length) {
    return []
  }
  // variables: x: array index, startCopy/endCopy: params for slicing. It is the
  // main objective of the function to set these values.
  // firstVal, lastVal: if values at the beginning/end of slice needs adjusting
  let x = 0, startCopy, endCopy
  let firstVal, lastVal

  // return format
  const finalize = () => {
    const s = []
    if (firstVal !== undefined) {
      s.push(firstVal)
    }
    if (startCopy !== undefined && endCopy !== undefined && endCopy - startCopy > 0) {
      s.push(row.slice(startCopy, endCopy))
    }
    if (lastVal !== undefined) {
      s.push(lastVal)
    }
    return s.flat()
  }

  // Find the first array member indicating an x value higher than the beginning of the slice
  while (row[x] < x1 && x < row.length) {
    x += 1
  }

  // If there are no forbidden values above the starting value of the slice, return empty
  if (x >= row.length) {
    return []
  }

  // The beginning of the slice:
  // An odd index means that the number indicates the ending of a run of forbidden pixels,
  // and thus the beginning of the slice lies within a run of forbidden pixels.
  if (x % 2) {
    // Perhaps we begin the slice exactly where a run ends. In that case, there is exactly
    // one pixel to include. In order not to break the format, we must include an onset x value
    // as well as its offset. Set the firstVal and the startCopy to the same thing
    if (row[x] === x1) {
      firstVal = row[x]
      startCopy = x
    }
    // If the ending of the run is above the beginning, we start the slice somewhere in the
    // middle of a run of forbidden pixels. Set the beginning of our slice as an onset, and
    // prepare to copy whatever comes after it.
    if (row[x] > x1) {
      firstVal = x1
      startCopy = x
    }
    // As per our loop logic, it cannot be the case that row[x] is lower than x1

    // An even index means the onset of a run, our slice begins either before the start of
    // a run of forbidden pixels or exactly at the start of one. In either case, just copy
    // whatever comes after it
  } else {
    startCopy = x
  }

  // Now that we have established the start index of copying, we can quickly check if
  // the end of the slice is further than the last run in the row ends. In that case,
  // we can simply copy the rest of it be finished early.
  if (x2 >= row[row.length - 1]) {
    endCopy = row.length
    return finalize()
  }


  // Now we look for the ending of the slice.
  while (row[x] <= x2 && x < row.length) {
    x += 1
  }

  // Our interests lies further than the array can manage, copy the rest of the array.
  if (x >= row.length) {
    endCopy = row.length
    return finalize()
  }

  // An odd number indicates that the next number of interest indicates the end of a run
  // of forbidden pixels
  if (x % 2) {
    // There is a chance that we have found the very last pixel of the run. In that case,
    // this is where we stop copying
    if (row[x] === x2) {
      endCopy = x
    }
    // If the end of the run is after our slice ends, we substitute for our slice ending
    if (row[x] > x2) {
      lastVal = x2
      endCopy = x
    }

    // An even number means that the next number is the beginning of a run
  } else {
    // If we happend upon the very beginning of the run, we must supply an end to not
    // break the format, and copy up until it to include both beginning and end of that
    // one single pixel run
    if (row[x] === x2) {
      lastVal = x2
      endCopy = x
    }

    // If the next start of the run is after our slice ends, we have now found all pixels
    // that interest us, and we can set to stop copying here, end not inclusive.
    if (row[x] > x2) {
      endCopy = x
    }
  }

  // That should cover all cases. Format the array and return
  return finalize()
}

export function extractForbiddenCoordsFromRFOOIRect(RFOOI, x1, y1, x2, y2) {
  const rect = sliceRFOOIToRect(RFOOI, x1, y1, x2, y2)
  return extractForbiddenCoordsFromRFOOI(rect, y1)
}

export function extractForbiddenCoordsFromRFOOIRectCENTERMODE(RFOOI, x, y, rectWidth, rectHeight) {
  const clampTo0 = v => v > 0 ? v : 0
  const x1 = Math.floor(clampTo0(x - rectWidth / 2))
  const y1 = Math.floor(clampTo0(y - rectHeight / 2))
  const x2 = Math.floor(clampTo0(x + rectWidth / 2))
  const y2 = Math.floor(clampTo0(y + rectHeight / 2))

  return extractForbiddenCoordsFromRFOOIRect(RFOOI, x1, y1, x2, y2)
}

export function getClosestObstacleWithinRange(RFOII, reference, w, h) {
  return getClosestObstaclesWithinRange(RFOII, reference, w, h, 1)[0]
}

export function getClosestObstaclesWithinRange(RFOOI, reference, w, h, n = 10) {
  const coords = extractForbiddenCoordsFromRFOOIRectCENTERMODE(RFOOI, reference.x, reference.y, w * 2, h * 2)

  const sorted = sortPointsByDistanceToReference(reference, coords)
  return sorted.slice(0, n)
}

export function sortPointsByDistanceToReference(reference, points) {
  //console.log(points)
  return points.sort((a, b) => pointDistance(a, reference) - pointDistance(b, reference))
}

export function pointDistance(p1, p2) {
  return Math.sqrt(
    Math.abs(
      Math.pow(Math.abs(p1.x - p2.x), 2) +
      Math.pow(Math.abs(p1.y - p2.y), 2)
    )
  )
}


export function decodeRunLengthImageSlice(rlImage, from, to) {
  return decodeRunLengthImage(sliceRunLengthImage(rlImage, from, to))
}

export function decodeRunLengthWithinRect(rlImage, x1, y1, x2, y2, width) {
  const from = x1 + y1 * width
  const to = x2 + y2 * width
  const rectWidth = x2 - x1
  const xInterval = width - rectWidth
  const yInterval = (y2 - y1) - 1
  const decoded = decodeRunLengthImage(sliceRunLengthImage(rlImage, from, to))
  for (let i = 0; i < yInterval; i += 1) {
    decoded.splice(from + rectWidth, xInterval)
  }
  return decoded
}

export function sliceRunLengthImageToYXMatrix(rlImage, width) {
  const l = rlImage.length
  const remaining = [...rlImage]
  const a = []
  for (let y = 0, n = l % width; y < n; y++) {
    a.push(new Uint8Array(sliceRunLengthImage(remaining, y * width, y * width + width)))
    a.push(slice)
  }
}

export function obstacleCoordsInRunLengthImage(rlImage, width) {

}

export function obstacleCoordsInRunLengthRect(rlImage, x1, y1, x2, y2, width) {
  const dec = decodeRunLengthWithinRect(rlImage, x1, y1, x2, y2, width)

}

export function getColFromImgData(imgData, x, y, width) {
  if (!imgData) {
    return [0, 0, 0, 255]
  }

  const i = x * 4 + y * width * 4

  return [
    imgData[i],
    imgData[i + 1],
    imgData[i + 2],
    imgData[i + 3],
  ]
}
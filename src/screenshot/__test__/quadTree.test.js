import fs from 'fs'
import path from 'path'
import * as PImage from 'pureimage'
import { fromImageData, searchTree } from '../quadTree.js'
import { compareArrayWithTolerance } from '../../utils/fuzzy.js'
import { atoobj } from '../../utils/jshelpers'

describe('quadtree search', () => {
    const white = [255, 255, 255]
    const black = [0, 0, 0]
    const red = [255, 0 , 0]
    const green = [0, 255, 0]
    const blue = [0, 0, 255]
    let file, img, ctx, testImgData

    beforeEach(async () => {
        file = fs.readFileSync(path.resolve() + '/src/screenshot/__test__/test.png')
        img = await PImage.decodePNGFromStream(fs.createReadStream(path.resolve() + '/src/screenshot/__test__/test.png'))
        ctx = img.getContext('2d')
        ctx.drawImage(img, 0, 0, img.width / 4, img.height / 4)
        testImgData = ctx.getImageData(0, 0, 25, 25)
    })

    const blackIsForbidden = (col) => {
        const isSortOfBlack = compareArrayWithTolerance(black, col, 15)
        return !isSortOfBlack
    }

    it('can store by callback', () => {
        const id = fromImageData(testImgData.data, 25, blackIsForbidden)
        const forbiddenPointsInRange = searchTree(id, 0 , 0, 8, 8)
        expect(forbiddenPointsInRange.length).toBe(6)
    })

    it('can store by color array', () => {
        const id = fromImageData(testImgData.data, 25, 15, [white, red, green, blue])
        const forbiddenPointsInRange = searchTree(id, 0 , 0, 8, 8)
        expect(forbiddenPointsInRange.length).toBe(6)
    })

    it('retrieved data are arrayed pairs', () => {
        const id = fromImageData(testImgData.data, 25, blackIsForbidden)
        const forbiddenPointsInRange = searchTree(id, 0 , 0, 8, 8)
        const firstCoords = forbiddenPointsInRange[0]
        expect(Array.isArray(firstCoords)).toBe(true)
        expect(firstCoords.length).toBe(2)
    })

    it('can format retrieved data through a callback', () => {
        const id = fromImageData(testImgData.data, 25, blackIsForbidden)
        const forbiddenPointsInRange = searchTree(id, 0 , 0, 8, 8, atoobj)
        const firstCoords = forbiddenPointsInRange[0]
        const { x, y } = firstCoords
        expect(typeof x).toEqual('number')
        expect(typeof y).toEqual('number')
    })
})
import { takeScreenshot, setupDebugImage, setupObstacleDrawing } from './takeScreenshot'
import { makeBoolImage, makeIntegralImage, encodeRowwiseForbiddenOnsetOffsetImage } from './imageUtils'
import * as quadtree from './quadTree.js'

export async function createBoolIntegralReferenceImage(width, height, allowedColors, target = document.body) {
  const screenshot = await takeScreenshot(width, height, target)
  const boolImage = makeBoolImage(screenshot, allowedColors)
  console.log(boolImage)
  return makeIntegralImage(boolImage, width, height)
}

export async function createRFOIIReferenceImage(width, height, allowedColors, target = document.body) {
  const screenshot = await takeScreenshot(width, height, target)
  return encodeRowwiseForbiddenOnsetOffsetImage(screenshot, width, allowedColors)
}

export { takeScreenshot, setupDebugImage, setupObstacleDrawing, quadtree }

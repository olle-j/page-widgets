export function arrayEquals(a, b) {
  for (let i = 0, l = a.length; i < l; i++) {
    if (!(a[i] == b[i])) {
      return false
    }
  }
  return true
}
